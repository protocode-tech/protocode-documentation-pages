---
title: Introduction
visible: true
taxonomy:
    category:
        - docs
---

Protocode Workspace est un espace de travail numérique dans lequel vous organiserez votre travail en projets, membres, tâches et assignations.

Chaque tâche assignée permet de démarrer un environnement de travail préconfiguré d'un simple clic par le développeur lui-même. Cet environnement sera installé dans un cloud, et il sera ensuite possible d'y travailler soit dans une interface web (_version portabilisée de Virtual Studio Code_), soit avec Virtual Studio Code installé en local sur sa machine (via une connexion ssh).

Chaque développeur disposera donc d'un environnement spécifique pour chaque tâche dont il a la charge (d'un même projet, ou de différents projets), et sur lesquels il peut se connecter rapidement. De cette manière, il pourra basculer de tâche en tâche sans avoir à reconfigurer son poste, ou intervenir très rapidement sur un problème à fixer en créant un nouvel environnement sur une tache dédiée. 

En fonction du paramétrage saisi (voir la partie [Virtualisation](/configuration/virtualization)), chaque environnement dispose d'une ou plusieurs urls de prévisualisation, qui sont accessibles au niveau de chaque assignation afin de pouvoir tester l'exécution du code produit par un développeur.

L'ensemble de son travail peut donc être testé instantanément et sans déploiement par toute personne supervisant les travaux.

Lorsque le travail est validé, il ne reste plus qu'à pousser les modifications et clôturer l'environnement.

