---
title: 'Interfaces web'
---

Dans Protocode, pour chaque environnement démarré, il existe différentes interfaces web qui vous permettent soit de coder, soit de tester. 

## Virtual Studio Code Web

Cet interface est fixe et existe toujours. Il s'agit d'une version portabilisée de Visual Studio Code. Il est possible d'y accéder depuis le menu contextuel d'une assignation via le raccourci "Ouvrir l'IDE web". Cette implémentation web de Virtual Studio Code offre les mêmes possibilités que la version logicielle. Il est possible d'y ajouter les mêmes extensions, d'ouvrir des terminaux, d'y télécharger des fichiers. En bref, d'y faire tout ce que l'on fait en local, mais au travers de la fenêtre de votre ordinateur.

C'est très pratique pour intervenir rapidement sur de la revue de code, ou pour permettre à d'autres développeurs de venir vous aider à débugger un problème (en leur fournissant l'url de l'interface).

## Les urls de prévisualisation

En fonction du paramétrage saisi dans la partie [Virtualisation](/configuration/virtualization) de chaque repository, certains conteneurs sont exposés. Protocode recense tous ces conteneurs, et liste les urls de prévisualisation associées. Celles-ci se trouvent dans le menu contextuel de chaque assignation.

! Bien que ces urls soient présentes dans le menu, cela ne veut pas pour autant dire que l'application associée soit en fonctionnement. Notamment, pour des applications javascript par exemple, où il faudra préalablement se connecter à l'environnement, et lancer la compilation depuis le conteneur node.

Ces urls de prévisualisation vont permettre tant au développeur qu'au superviseur de la tâche de venir tester l'exécution du code produit, sans qu'il soit nécessaire de faire un déploiement. C'est aussi un outil très pratique lorsque l'on développe une application qui communique de manière bilatérale avec des outils tiers (tels que les services de paiement en ligne) souvent au travers de webhooks.