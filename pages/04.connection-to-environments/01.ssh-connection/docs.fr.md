---
title: 'Connexion en ssh'
---

Une fois un environnement démarré, il est possible de s'y connecter en ssh. Cette option apparait dans le menu contextuel "Coder" se trouvant sur chaque assignation de tâche où un environnement est ouvert. Au clic sur cette option, une modale s'ouvre, et vous propose différents cas de figure. 

## Se connecter en SSH (depuis son terminal)

Dans le menu contextuel "Coder" d'une assignation, il vous est proposer de "Se connecter en SSH". Cette option ouvre une modale dans laquelle vous trouverez une ligne de commande à copier, permettant de se connecter en SSH depuis votre terminal, du type: 

[prism classes="language-bash"]
ssh protocode@e1.xxx.protocode.cloud -p 2222
[/prism]

Il ne vous reste plus qu'à copier la commande et la coller dans votre terminal.

## Se connecter depuis Visual Studio Code

Dans le menu contextuel "Coder" d'une assignation, vous disposez d'un bouton vous permettant d'"Ouvrir dans VSCode". Ce raccourci ouvrira directement l'extension [Remote - SSH](https://code.visualstudio.com/docs/remote/ssh-tutorial) de votre VSCode local. Si l'extension n'est pas installée, il vous sera proposé de l'installer préalablement.

## Se connecter depuis les IDEs JetBrains

Dans le menu contextuel "Coder" d'une assignation, il est possible de connecter un environnement avec l'un des 8 IDEs de JetBrains suivants:

* CLion
* GoLand
* IntelliJ
* PhpStorm
* PyCharm
* Rider
* RubyMine
* WebStorm

!!! Pour cela, il est indispensable d'installer préalablement l'utilitaire [JetBrains Gateway](https://www.jetbrains.com/fr-fr/remote-development/gateway/).

## Clé ssh personnelle et mot de passe

Dans les cas vus précédemment, si vous n'avez pas fournis à Protocode de clé ssh personnelle, il vous sera demandé de fournir un mot de passe. Celui-ci se trouve en bas de la modale ouverte avec l'option "Se connecter en ssh". Ce dernier a été généré aléatoirement lors de l'ouverture de l'environnement.

Si vous ne souhaitez pas avoir à fournir de mot de passe chaque fois que vous vous connectez à un environnement en ssh, vous avez la possibilité d'enregistrer une (ou plusieurs) clés ssh. Pour cela, il vous faut aller dans la barre de navigation en haut, cliquer dans le menu contextuel, puis cliquer sur "Clés SSH". Une modale s'ouvre, et vous offre la possibilité d'entrer une nouvelle clé dans un formulaire. En dessous de celui-ci, se trouve toutes les clés que vous avez déjà enregistré.

Si vous n'avez encore jamais généré de clé ssh sur votre poste, il va falloir le faire préalablement. 

! Si vous travaillez sur **Windows**, il vous faudra tout d'abord **vérifier que l'application OpenSSH est installée**.

Si vous travaillez sur Linux ou Mac, entrez directement dans votre terminal:

[prism classes="language-bash"]
ssh-keygen -t rsa -b 4096 -C "firstname.lastname@provider.ext"
[/prism]

Dans cette commande, il vous faut bien évidemment remplacer _firstname.lastname@provider.ext_ par votre adresse email.

Une fois la clé générée, affichez la dans votre terminal en lançant:

[prism classes="language-bash"]
cat ~/.ssh/id_rsa.pub 
[/prism]

Il ne vous reste plus qu'à la copier, et la coller dans le formulaire de la modale.