---
title: 'Connexion aux environnements'
---

# Chapitre 3 - Connexion aux environnements

Dans Protocode, il existe plusieurs manières de communiquer avec les environnements.

Soit pour coder, et il sera possible de le faire depuis avec une version web de Visual Studio Code, ou avec celui installé sur sa machine (en se connectant en ssh).

Soit pour tester, et il sera alors possible d'accéder au rendu grâce des urls de prévisualisation.