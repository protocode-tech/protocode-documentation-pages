---
title: 'La préconstruction'
---

# Pré-construction

Lorsque un repository été ajouté et sa virtualisation configurée, il est ensuite nécessaire de le préconstruire. Autrement dit, de construire et de stocker toutes les images utilisées par Docker, ainsi que de veiller à ce que toutes les commandes saisies dans le cycle de vie d'un environnement se lancent correctement. 

## Préconstruction pure

Dans l'onglet Préconstruction de la modale d'édition d'un repository, vous trouverez deux blocs. L'un est la "Dernière préconstruction lancée", l'autre est le "Dernier cycle complet lancé".

Concernant ce premier bloc, il s'agit d'un processus qui va démarrer un environnement, construire toutes les images saisies dans le fichier docker-compose, et les stocker dans un registry privé. Ainsi, il sera très rapide de démarrer les environnements qui pourront s'appuyer sur des images déjà construites, plutôt que de les construire au démarrage.

Ce n'est évidemment utile que si votre repository dispose de ses propres fichiers Dockerfile, et ne repose pas uniquement sur des images préconstruites et hébergées par exemple sur le hub de Docker. Si votre repository dispose de ses propres images à construire, il faudra donc penser à relancer une préconstruction à chaque fois que vous modifiez un fichier Dockerfile ou des fichiers utilisés lors de la construction de l'image, afin que tout environnement ouvert par la suite ait une image à jour.

Lors qu'un processus de préconstruction est lancé, c'est asynchrone. Vous pouvez, si vous le souhaitez, fermer la modale ou même la fenêtre. Le statut de la préconstruction d'une source est d'ailleurs affiché dans la liste des sources de votre projet. 

Il y a 5 statuts possibles à cette préconstruction: 

* **En attente**: le processus n'a pas encore été lancé.
* **En cours**: le processus est en cours d'exécution.
* **Terminé**: le processus est terminé avec succès.
* **En erreur**: le processus a rencontré une erreur.
* **Arrêté**: le processus a été manuellement arrêté.

Les logs de sortie sont visibles à tout moment dans la modale d'édition d'une source, lors de l'exécution du processus comme après. Ce qui vous permet de vous debugger facilement. 

Une fois qu'une pré-construction aura été faite avec succès pour toutes les sources du projet, les onglets "Équipe" et "Tâches" seront alors actifs, et vous pourrez commencer à constituer votre équipe, et organiser le travail à faire en créant des tâches et en les assignant aux différents développeurs de votre équipe.

## Lancement d'un cycle complet

Ce processus permet de tester le démarrage et la fermeture d'un environnement en passant par toutes les étapes de son cycle de vie en utilisant les images déjà construites, et donc de tester très rapidement toute modification dans l'onglet "Virtualisation" des scripts du cycle de vie.

Dans son fonctionnement, c'est une mécanique identique à celle du processus de préconstruction. Celui-ci est aussi asynchrone, le statut a les mêmes valeurs, et les logs peuvent être consultés dans la modale d'édition du repository concernés pendant ou après son exécution.

## Spécificités de l'exécution des scripts du cycle de vie

Dans l'onglet "Virtualisation", au niveau de chaque cycle de vie se trouvent des badges pouvant avoir les valeurs suivantes:

* **Préconstruction**: ce script sera exécuté au moment de la préconstruction.
* **Démarrage**: ce script sera exécuté au démarrage d'un environnement (qui se produira au lancement d'un cycle complet).
* **Fermeture**: ce script sera exécuté à la fermeture d'un environnement (qui se produira à la fin d'un cycle complet).