---
title: 'Connexion aux Sources Git'
---

Pour la connexion aux sources, vous devez;
•	Identifier les sources:
    Le remplissage d'un nom et de l'url de celui-ci. L'url peut être celle d'un repository hébergé sur GitHub, mais aussi GitLab (public ou privé), ou encore BitBucket. Nous vous recommendons de fournir l’adresse utilisée en ssh, par exemple git@githost.ext:me/my-repository.git. 
> Si vous ne pouvez pas vous connecter en ssh ; alors il faudra fournir dans l'url l'utilisateur et le mot de passe. Par exemple https://user:password@githost.ext/me/my-repository.git. 
> C'est cependant fortement déconseillé, car ces identifiants de connexion seront visibles par toutes les personnes accédant aux environnements ouverts.

•	Attribuer les droits en lecture et écriture à Workspace. (Etape non necessaire si connexion sans SSH). 
Protocode va générer une clé ssh qu'il faudra reporter en tant que clé de déploiement ayant les droits en écriture. 
•	Choisir la branche par défaut: celle sur laquelle sera positionné l'utilisateur à l'ouverture d'un environnement.
Une fois ces étapes remplies, Workspace est capable de récupérer et pousser des modifications vers le code source, mais il n'est pas encore en mesure de l'exécuter. Cette partie relève de l'étape suivante: la virtualisation