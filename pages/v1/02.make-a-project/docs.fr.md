---
title: 'Créer un Projet'
---

L'une des idées fondatrices de Protocode Workspace est d'automatiser l'installation des projets, de manière à ne dépenser ce temps qu'une fois, plutôt que de réitérer cette étape à l'arrivée de chaque nouveau développeur, ou au remplacement d'une machine.
Pour ce faire, vous disposez d'interfaces web vous permettant pas à pas de configurer votre projet, de la réception du code source à la virtualisation, mais aussi d'y intégrer des scripts d'initialisation.
Cliquer sur Créer en haut à droite puis renseigner les champs suivants
* Details
Renseigner le nom, et la description ainsi que les ressources que vous souhaitez allouer aux environnements du projet.

* Repositories
Dans cette section, vous allez définir le projet dans son ensemble.

* Equipe
Dans cette section, vous ajoutez les membres que vous souhaitez voir intervenir sur le projet et leur définissez leurs droits d'acces. 

* Tâches
Enfin, dans cette section, vous pouvez commencer à ajouter les tâches et les assigner aux membres de votre équipe. 
