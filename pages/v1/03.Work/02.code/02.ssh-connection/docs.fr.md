---
title: 'Connexion en ssh'
---

Vous pouvez soit [Utiliser votre clé SSH personnelle](#utiliser-ma-cle-ssh-perso)  (**recommandé**) ou [Utiliser le mot de passe aléatoire généré par Workspace pour votre environnement.](#utiliser-le-mot-de-passe-) 


# Utiliser ma clé ssh personnelle 

## Générer une clé SSH
Si vous avez déjà une clé SSH, passez à l'étape suivante. 
Sinon, vous devez générer une clé ssh sur votre poste. 

! Sur **Windows**, il vous faudra tout d'abord **vérifier que l'application OpenSSH est installée**.

Sur Linux ou Mac, entrez directement dans votre terminal:

[prism classes="language-bash"]
ssh-keygen -t rsa -b 4096 -C "firstname.lastname@provider.ext"
[/prism]

Dans cette commande, remplacer _firstname.lastname@provider.ext_ par votre adresse email.

Une fois la clé générée, affichez la dans votre terminal en exécutant:

[prism classes="language-bash"]
cat ~/.ssh/id_rsa.pub 
[/prism]

Il ne vous reste plus qu'à la copier- coller dans le formulaire de la modale.

## Enregistrer la clé SSH. 
Pour enregistrer votre clés ssh : 
Dans la barre de navigation en haut à droite, cliquez sur le menu contextuel, puis sur "Clés SSH". 
Copier coller la clé générée à l'étape précédente puis sauvegardez.



# Utiliser le mot de passe généré pour l'environnement

Si vous n'avez pas ajouté de clé SSH, alors vous devrez renseigner le mot de passe aléatoire généré par Workspace. Pour cela:
* Cliquer sur "Se connecter en SSH" puis,
* Cliquer sur "show password" en bas puis,
* Cliquer sur "se connecter avec VSCode". 
_La modale vous informant que vous n'avez pas de clé SSH s'ouvre, passez à l'étape suivante, 

* A l'ouverture de VScode, acceptez la connexion au serveur puis, 
* Copiez collez l'identifiant et le mot de passe. 
* Enfin approuvez la dernière modale pour 'faire confiance aux auteurs'.

Vous pouvez également lancer la connexion depuis VS Code 

Ouvrez une nouvelle fenêtre, cliquez en bas à gauche sur le raccourcis "Ouvrir une fenêtre distante" (qui est rajouté par l'extension), puis  sélectionner "Connect to host" et coller l'url fournie par Workspace de la forme
[prism classes="language-bash"]
root@e1.s14.protocode.tech:2222
[/prism]
Enfin entrez le mot de passe généré par Workspace. 

Une fois un environnement démarré, il est possible de s'y connecter en ssh. Cette option apparait dans le menu contextuel sur chaque assignation de tâche où un environnement est ouvert. Au clic sur cette option, une modale s'ouvre, et vous propose différents cas de figure. 

# Se connecter depuis son terminal

Il est possible de se connecter à l'environnement depuis son terminal. 
Ouvrez un terminal et executez  

[prism classes="language-bash"]
ssh root@e1.xxx.protocode.cloud -p 2222
[/prism]

en modifiant @e1.xxx.protocode.cloud par le bon lien.
