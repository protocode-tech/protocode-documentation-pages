---
title: 'Anatomie d''un environnement'
---

## Arborescence des fichiers

Le **répertoire par défaut** d'un environnement est ` /var/environment`. Vous y êtes automatiquement positionné lorsque vous vous connectez en ssh avec Visual Studio Code ou votre terminal, et lorsque vous ouvrez l'interface web de Visual Studio Code.

À l'intérieur du répertoire /var/environment, se trouvent les différents repositories qui composent votre projet:

[prism classes="language-text" highlight="2,3"] 
└── /var/environment/
   ├── my-repository/
   └── my-other-repository/
[/prism]


Lorsqu'un environnement est démarré, Protocode va générer un dossier `.protocode-runtime` qui sera stocké à la racine de chaque repository du projet. Ce dossier contient deux fichiers: `.env` et `docker-compose.yml`. Vous aurez donc dans l'arborescence des fichiers:

[prism classes="language-text" highlight="3"] 
└── /var/environment/
   ├── my-repository/
   │   └── .protocode-runtime/
   │      └── .env 
   │      └── docker-compose.yml
   │ ... Rest of my repository files
[/prism]

Ces fichiers ont les rôles suivant:

* **`.env`**: ce fichier contient toutes les variables d'environnement saisies dans la configuration du repository. Ces variables seront injectées dans chaque commande de docker-compose.

* **`docker-compose.yml`**: c'est le fichier de configuration généré par Protocode à destination de docker-compose. C'est lui qui sera pris en compte lorsque vous ferez appel à docker-compose depuis la racine d'un repository (même si un autre fichier docker-compose.yml se trouve au même niveau). Vous pourrez donc, par exemple, stopper tous les conteneurs avec une commande telle que:

[prism classes="language-bash command-line" cl-prompt="$"]
cd my-repository # Je me déplace du répertoire par défaut au repository
docker-compose down
[/prism]

Puis éventuellement modifier certaines valeurs du fichier de démarrage (.protocode-runtime/docker-compose.yml), et relancer les conteneurs en lançant: 

[prism classes="language-bash command-line" cl-prompt="$"]
cd /var/environment/my-repository
docker-compose up -d
[/prism]

!! Le dossier **.protocode-runtime est ignoré globalement** dans git et n'apparaitra pas lorsque vous afficherez les modifications au sein de chacune des sources.
!! **Il est important de ne pas le supprimer** pour le bon fonctionnement de notre système.

## Outils préinstallés

Un environnement est une instance Debian 11 qui embarque un certain nombre d'outils nécessaires à son fonctionnement, notamment:

- **Pour la virtualisation**: docker, docker-compose
- **Pour le versionning des sources**: git, git-lfs
- **Pour le terminal**: bash, zsh
- **Pour l'édition de fichiers**: vim, nano
- **Pour la récupération de fichiers**: curl, wget

!!! Vous pouvez selon vos besoins utiliser ces outils ou en installer (par exemple make). Cependant, ne perdez pas de vue qu'un environnement est une machine destinée à faire tourner des conteneurs Docker. Il est donc par exemple inutile d'y installer node. On préfèrera ajouter un conteneur faisant tourner node.

## Conteneurs docker

Votre environnement est comme un poste de travail distant faisant tourner des conteneurs avec Docker. Lorsque vous vous connectez, vous êtes positionné dans le répertoire par défaut et à l'extérieur de vos conteneurs. Si vous ouvrez un terminal et entrez la commande suivante pour voir la liste de vos conteneurs:

[prism classes="language-bash command-line" cl-prompt="$"]
docker ps
[/prism]

Vous verrez un retour du type: 

[prism classes="language-bash"]
CONTAINER ID   IMAGE              COMMAND                  CREATED       STATUS       PORTS                 NAMES
3a70f7c8c765   node:16            "docker-entrypoint.s…"   10 days ago   Up 10 days   80/tcp                protocode-web_app
6703eddacc5d   protocode-api_app  "docker-php-entrypoi…"   3 weeks ago   Up 3 weeks   80/tcp, 9000/tcp      protocode-api-app
82c3d52d0e50   mysql:8.0          "docker-entrypoint.s…"   3 weeks ago   Up 3 weeks   3306/tcp, 33060/tcp   protocode-api-database
[/prism]

Comme vous êtes dans le répertoire par défaut, et non à l'intérieur d'un de vos repositories, toute commande docker-compose déboucherait sur une erreur, puisque que docker-compose ne trouverait pas de fichier de configuration dans le répertoire par défaut. Il vous faut donc tout d'abord vous déplacer, pour ensuite lancer une commande docker-compose:

[prism classes="language-bash command-line" cl-prompt="$"]
cd my-repository
docker-compose exec app bash # Pour cet exemple, j'entre dans le terminal d'un conteneur
[/prism]

En somme, ce n'est pas différent d'une utilisation de Docker en local.

## Git

Quand à Git, celui-ci est installé au niveau de l'environnement, et a été paramétré pour communiquer avec les différents repositories qui compose votre projet, afin de récupérer les sources et pousser des modifications.

**Par défaut, chaque repository est positionné sur la branche sélectionnée comme "Par défaut"** dans sa configuration (voir [Ajout de source](/configuration/sources)). Il revient au développeur, comme en local, de créer sa propre branche de travail sur chaque repository et de se mettre à jour (via git pull / git rebase selon vos habitudes). 

! Pour éviter que des modifications ne soient poussées sur la branche principale d'un repository, nous vous invitons à veiller à ce qu'elle soit protégée du coté de votre hébergeur de source, et ne puisse être modifiée qu'au travers d'une fusion.