---
title: Coder
---

Afin de coder dans un environnement, lancer un éditeur depuis le menu contextuel d'une assignation (cf. image). 
Vous pouvez au choix, utiliser [l'IDE en ligne](#ide-en-ligne) ou utiliser votre [VS Code local](#votre-vs-code-local).

![ContextuelassignationAgent](ContextuelassignationAgent.png "ContextuelassignationAgent")


## IDE en ligne

Chaque environnement dispose d'une version portabilisée de Visual Studio Code. 
Cette implémentation web de VS Code offre les mêmes possibilités que la version logicielle. 
Il est notamment possible d'y ajouter les mêmes extensions*, d'ouvrir des terminaux, d'y télécharger des fichiers. 
En bref, d'y faire tout ce que l'on fait en local, mais au travers de la fenêtre de votre navigateur.

l'IDE en ligne **est recommendé** pour intervenir rapidement sur la revue de code ou le debug rapide (en leur fournissant l'url de l'interface).

**Attention aux Extensions VSCode **
_* Les extensions ne sont pas la responsabilité de Protocode ; elle peuvent présenter des comportements problématiques telle que des fuites mémoires. Nous recommendons d'en limiter l'usage ou de d'augmenter les ressources de vos environnements._

## Votre VS Code local
Depuis le menu contextuel, 
* cliquez sur  "Lancer VSCode" qui exécutera l'extension VSCode  [Remote - SSH](https://code.visualstudio.com/docs/remote/ssh-tutorial) avec l'url fournie par Workspace. 
_Si l'extension n'est pas installée, il vous sera proposé de l'installer._


* [Connectez VSCode à l'environnement de façon sécurisée.](/v1/work/code/ssh-connection)

**Attention aux Extensions VSCode **
_* Les extensions ne sont pas la responsabilité de Protocode ; elle peuvent présenter des comportements problématiques telle que des fuites mémoires. Nous recommendons d'en limiter l'usage ou de d'augmenter les ressources de vos environnements._
