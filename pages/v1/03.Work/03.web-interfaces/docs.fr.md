---
title: 'Interfaces web'
---

Dans Protocode, pour chaque environnement démarré, il existe différentes interfaces web qui vous permettent soit de coder, soit de tester. 


## Les urls de prévisualisation

En fonction du paramétrage saisi dans la partie [Virtualisation](/configuration/virtualization) de chaque repository, certains conteneurs sont exposés. Protocode recense tous ces conteneurs, et liste les urls de prévisualisation associées. Celles-ci se trouvent dans le menu contextuel de chaque assignation.

! Bien que ces urls soient présentes dans le menu, cela ne veut pas pour autant dire que l'application associée soit en fonctionnement. Notamment, pour des applications javascript par exemple, où il faudra préalablement se connecter à l'environnement, et lancer la compilation depuis le conteneur node.

Ces urls de prévisualisation vont permettre tant au développeur qu'au superviseur de la tâche de venir tester l'exécution du code produit, sans qu'il soit nécessaire de faire un déploiement. C'est aussi un outil très pratique lorsque l'on développe une application qui communique de manière bilatérale avec des outils tiers (tels que les services de paiement en ligne) souvent au travers de webhooks.