---
title: 'Les tâches'
---

À l'intérieur d'un projet, il est possible de créer autant de tâche que souhaité. Celles-ci se retrouvent dans la fiche d'un projet, dans l'onglet "Tâches".
Chaque tâche créée dispose d'un certain nombre d'informations 
* Le nom, la description et le flag (fonctionnalité/bug) de la tâche.
* L'état (ouvert/fermé), 
* La priorité (de basse à urgente), 
* Les dates prévues de début et de fin, 
* Le temps prévu pour réaliser le travail,
* La personne en charge de la supervision de la tâche (le "Superviseur").
 
Par défaut une tâche créée sera identifiée comme "Non assignée".
Pour l'assigner à un membre, Cliquer sur la tache depuis la liste des tâche), puis  cliquer "Assigner" et selectionner l'un des membres de l'équipe. 

Une fois l'assignation créée, la tâche dispose alors d'un statut "En attente". Parallèlement, la personne assignée à cette tâche recevra un mail de notification pour l'en informer. Lorsqu'elle se connectera à Protocode, la tâche apparaitra dans "Mes tâches", et elle pourra démarrer un environnement spécifique à celle-ci.

