---
title: 'Organisation d''''un Workspace'
---

Vous venez de [créer votre workspace ](https://protocode.tech/apply) ou d'être invité dans un workspace.
Un workspace se décompose en 4 zone distinctes : 
1. Une zone d'acces rapide personalisée vous permettant de voir vos tâches et supervisions en cours ainsi que, le cas échéant, l'administration de votre Workspace (les membres et les informations de facturation). Note, nous ne sauvegardons pas vos informations de paiement. Ces dernières sont confiés à l'expert mondial du paiement en ligne Stripe.  
2. Une zone d'aide, vous permettant d'acceder rapidement aux ressources de Workspace. 
3. La zone active qui se modifie en fonction de votre selection. 
4. Vos informations personnelles et préférences de langue. 

Notez aussi la présence sur chaque écran workspace du bouton feedback à droite permettant d'envoyer en 2 clics un feedback directement à l'équipe support. 

![workspaceImage](workspaceImage.png "workspaceImage")

Vous devez maintenant administrer les membres de votre workspace -c'est à dire définir les membres de votre entreprise ou externes susceptibles d'intervenir et les rôles / permissions associés. 
Pour ce faire, rdv dans l’onglet « membres » du panel de gauche.

Afin de s’intégrer aisément aux équipes, méthodes et process de développement modernes, le travail au sein de Workspace s’organise par Projets. Un projet est composé de tâches. 

Toute personne ajoutée en tant que membre de l'espace de travail pourra ensuite être ajoutée dans une équipe liée à un projet.

