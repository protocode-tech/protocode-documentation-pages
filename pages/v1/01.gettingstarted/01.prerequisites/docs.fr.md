---
title: Prérequis
---

Afin d'utiliser Workspace, vous devez,

1. Héberger votre code sur un ou plusieurs repository GIT.
Workspace est compatible avec Github, Gitlab et Bitbucket. 
[Vous ne savez pas lequel choisir?](https://jelvix.com/blog/bitbucket-vs-github-vs-gitlab )


3. Posséder une connexion internet afin de vous connecter à la plateforme.
Et bien oui, c'est un service cloud, pas d'internet, pas d'acces. 

6. Maîtriser Docker 
Il est fortement recommandé de maîtriser les concepts de Docker. 
[Vous voulez apprendre Docker?](https://docs.docker.com )

5. Posséder une machine physique permettant de lancer un navigateur Chrome, Firefox ou Safari par exemple

1.  Savoir coder dans Visual Studio Code
Il est toujours utile de maîtriser un editeur de Code, Workspace a fait le choix de s'interfacer avec le plus famillier des IDE : VS Code.
Vous ne l'avez pas encore? Pas de souci, vous pouvez [le télécharger](https://code.visualstudio.com) ou même directement coder dans votre navigateur sans rien installer.
