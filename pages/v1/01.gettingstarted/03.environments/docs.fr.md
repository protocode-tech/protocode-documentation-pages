---
title: 'Les environnements'
---

# Contenu d'un environnement

Lorsque vous utilisez workspace, et lancer une tache, un environnement est crée dans le cloud. L'environnement est une instance Debian 11 qui embarque Docker, et dans laquelle se trouvent des conteneurs créés grâce aux éléments saisis dans la partie virtualisation de chaque repository du projet.

Cette instance dispose de base des outils nécessaires à son fonctionnement, notamment:
- **Pour la virtualisation**: docker, docker-compose
- **Pour le versionning des sources**: git, git-lfs
- **Pour le terminal**: bash, zsh
- **Pour l'édition de fichiers**: vim, nano
- **Pour la récupération de fichiers**: curl, wget

Chaque environnement dispose d'un cycle de vie, autrement dit d'évènements qui rythment son existence, et sur lesquels il est possible de saisir des commandes à lancer. Ce peut être pour ajouter des dépendances au sein de l'environnement (ex: installer make), dupliquer des fichiers .dist, créer des variables, installer des dépendances au sein des conteneurs. En bref, automatiser toute opération nécessaire pour que l'environnement soit en état d'être utilisé une fois démarré.

# Ressources allouées
Avec Workspace, vous pouvez définir les ressources à allouer à vos environnements démarrés en fonction des besoins de chacun de vos projets.
Les ressources disponibles sont les suivantes 

| Configuration | CPU | Ram | Espace disque | Par défaut |
| ----------------- | ------- | ----- | ----------------- | ------------ |
| Small | 2 coeurs | 4 Go | 30 Go SSD | Oui |
| Medium | 4 coeurs | 8 Go | 60 Go SSD | Non |
| Large | 8 coeurs | 16 Go | 120 Go SSD | Non |
| XLarge | 16 coeurs | 32 Go | 240 Go SSD | Non |
| XXLarge | 32 coeurs | 64 Go | 300 Go SSD | Non |

!!! Toute modification des ressources à allouer n'impactera pas les environnements déjà ouverts, mais uniquement les environnements ouverts postérieurement.


# Quota
Lors de votre création de workspace, vous bénéficiez d’un quota par défaut limitant le nombre d’environnement que vous pouvez ouvrir en parallèle. Afin d’éditer ce quota, rapprochez vous de votre responsable commercial contact@protocode.tech  


# Pour aller plus loin
* [L'anatomie d'un environnement](/environment-anatomy)
* [Se connecter en SSH](/ssh-connection)
* [L'interface Web](/web-interfaces)


