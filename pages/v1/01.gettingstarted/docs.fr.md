---
title: 'Débuter avec Workspace'
media_order: 'schéma Workspace_v1.png'
---

## C'est quoi Workspace?

Workspace est une plateforme de développement numérique en ligne pour les équipes de développement.

Workspace vous permet d'ouvrir des environnements distants préconfigurés - tâche par tâche pour vos équipes de développement.

Workspace est efficace (il fait gagner en moyenne 15% de temps aux équipes de développement) et responsable (l'infrastructure cloud est hebergée en France et en Europe et alimentée en énérgie 100%d'origine renouvelable).

Grâce à workspace, vous ne serez plus dépendant de vos configurations matérielles, des pannes de vos ordinateurs ou des configurationsDrifts de vos projets. 


## Comment ça marche Workspace?

Dans Workspace, les développements d'une organisation s'organisent en projets et se découpent en tâches.  Les taches sont crées, assignées,  effectués, revues et validés par les membres que vous définissez.

Chaque tâche assignée permet de démarrer un environnement de travail préconfiguré d'un simple clic par le développeur lui-même. Cet environnement est installé dans le cloud, et il est ensuite possible d'y travailler soit dans une interface web (version portabilisée de Visual Studio Code), soit avec Visual Studio Code installé en local sur sa machine (via une connexion ssh).

![sche%CC%81ma%20Workspace_v1](sche%CC%81ma%20Workspace_v1.png "sche%CC%81ma%20Workspace_v1")

## Codez et testez instantanément
Chaque développeur dispose donc d'un environnement spécifique pour chaque tâche dont il a la charge.

De cette manière, il peut basculer de tâche en tâche sans avoir à reconfigurer son poste, ou intervenir très rapidement sur un problème à fixer en créant un nouvel environnement sur une tache dédiée. 

En fonction du paramétrage saisi, chaque environnement dispose d'une ou plusieurs urls de prévisualisation, qui sont accessibles au niveau de chaque assignation afin de pouvoir tester l'exécution du code produit par un développeur.

L'ensemble de son travail peut donc être testé instantanément et sans déploiement par toute personne supervisant les travaux.

Lorsque le travail est validé, il ne reste plus qu'à pousser les modifications et clôturer l'environnement.

## Vous etes prêt? 
[Vérifier les pré-requis](https://learn.protocode.tech/fr/v1/gettingstarted/prerequisites)