---
title: 'Organisation du travail'
taxonomy:
    category:
        - docs
---

# Chapitre 2 - L'organisation du travail

Dans Protocode Workspace, le travail est organisé en projets, contenant des tâches, assignées à des membres de l'équipe réunie autour du projet.