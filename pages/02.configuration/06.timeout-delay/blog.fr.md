---
title: 'Mise en sommeil'
---

Par défaut, les environnements sont ouverts indéfiniment sans contrainte de durée. Mais il est recommandé d'y associer un délai de mise en sommeil, de sorte qu'un environnement qui n'est plus utilisé soit automatiquement stoppé, et arrête de consommer des ressources serveur sans raison.

## Configuration / déclenchement

Cette configuration est possible depuis la fiche d'un projet, dans l'onglet "Détails", en modifiant la valeur du champ "Temps d'inactivité avant mise en sommeil". La valeur est exprimée en minutes.
![Capture%20d%E2%80%99e%CC%81cran%202022-09-30%20a%CC%80%2010.07.54](Capture%20d%E2%80%99e%CC%81cran%202022-09-30%20a%CC%80%2010.07.54.png "Capture%20d%E2%80%99e%CC%81cran%202022-09-30%20a%CC%80%2010.07.54")

Il est aussi possible de déclencher cette action manuellement au niveau d'une tâche assignée.
![Capture%20d%E2%80%99e%CC%81cran%202022-09-30%20a%CC%80%2010.07.20](Capture%20d%E2%80%99e%CC%81cran%202022-09-30%20a%CC%80%2010.07.20.png "Capture%20d%E2%80%99e%CC%81cran%202022-09-30%20a%CC%80%2010.07.20")

!!! Est considéré comme inactif un environnement qui ne dispose d'aucune connexion ssh et http active.

## Effets

La mise en sommeil aura pour effet de sauvegarder toutes les données se trouvant dans le répertoire `/var/environment`, et de supprimer l'environnement. Elle aura aussi pour effet d'arrêter la facturation de l'environnement.

La remise en marche de l'environnement viendra quant à elle remettre les données au sein du répertoire `/var/environment`, puis redémarrera les conteneurs de chaque sous projet.

!! Il est important de noter que les volumes déclarés dans les fichiers docker-compose ne seront pas sauvegardés s'ils ne pointent pas un répertoire se trouvant dans `/var/environment`.

!!! Toute modification du temps d'inactivité avant mise en sommeil n'impactera pas les environnements déjà ouverts, mais uniquement les environnements ouverts postérieurement.