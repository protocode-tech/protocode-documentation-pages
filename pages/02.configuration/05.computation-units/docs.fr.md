---
title: 'Unités de calcul'
---

Au niveau de chaque projet dans l'onglet "Détails", il est possible de déterminer les ressources à allouer pour tout environnement démarré en fonction des besoins du projet.

Les ressources sont découpées en "Unité de calcul", de cette manière:

| Nombre d'unités | CPU | Ram | Espace disque | Par défaut |
| ----------------- | ------- | ----- | ----------------- | ------------ |
| 1 | 2 coeurs | 4 Go | 30 Go SSD | Oui |
| 2 | 4 coeurs | 8 Go | 60 Go SSD | Non |
| 4 | 8 coeurs | 16 Go | 120 Go SSD | Non |
| 8 | 16 coeurs | 32 Go | 240 Go SSD | Non |
| 16 | 32 coeurs | 64 Go | 300 Go SSD | Non |

!!! Toute modification des ressources à allouer n'impactera pas les environnements déjà ouverts, mais uniquement les environnements ouverts postérieurement.


