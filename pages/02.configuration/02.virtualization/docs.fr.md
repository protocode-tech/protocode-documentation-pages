---
title: Virtualisation
visible: true
taxonomy:
    category:
        - docs
---

Lorsqu'un repository a été ajouté, et que les droits en lecture et écriture ont bien été accordés à Protocode, reste ensuite à gérer la virtualisation. Autrement dit, gérer les dépendances nécessaires pour son exécution. Par exemple, la nécessité d'avoir Php installé, voire MySQL ou encore NodeJS.

Pour ce faire, Protocode se repose sur  [Docker](https://www.docker.com/), et plus particulièrement sur [Docker-compose](https://docs.docker.com/compose/), le gestionnaire de conteneurs multiples. Ce qui vous permet de faire tourner Protocode même sur des projets complexes, contenant notamment des micro-services.

Un environnement peut contenir un, ou plusieurs repositories, provenant d'un même gestionnaire de source ou de plusieurs. On peut donc imaginer par exemple, faire tourner un projet contenant une api en php hébergée chez GitHub et un front en javascript hébergé chez GitLab dans un seul et même environnement.

Un environnement n'est en soi rien de plus qu'une instance Debian 11 faisant tourner Docker, et qui démarrera des conteneurs grâce à Docker-compose pour chacun des repositories qu'elle contient. _Par exemple: un conteneur fera tourner Php et une base de données pour le repository A, un autre fera tourner Node pour le repository B._

Il faudra donc établir une configuration Docker-compose pour chacun des repositories. Dans cette optique, l'outil de configuration de Protocode vous propose de choisir entre 3 stratégies:

* **Écrire un fichier docker-compose en ligne**: c'est la solution la plus adaptée si votre projet n'a pas de complexités particulières, et que vous pouvez vous reposer sur des images préconstruites (notamment de [Docker hub](https://hub.docker.com/)).

* **Utiliser un fichier docker-compose présent dans votre repository**: c'est la solution la plus adaptée si vous avez besoin de créer des conteneurs basés sur des images créées de toute pièce pour répondre à des besoins précis.

* **Utiliser des images préconstruites de Protocode**: cela vous permet de créer une configuration docker-compose sans écrire une ligne, par un jeu de sélection de services principaux (Php, Node, etc) et secondaires (base de donnée, etc). C'est la solution la plus adaptée si vous n'êtes pas encore à l'aise avec Docker.

---

## L'édition du fichier docker-compose destiné à Protocode:

L'édition d'un fichier docker-compose destiné à Protocode est presque identique à celle d'une utilisation locale. Vous pouvez donc vous reposer sur la configuration telle que décrite dans la [documentation officielle](https://docs.docker.com/compose/). Il est d'ailleurs tout à fait possible qu'une configuration déjà existante fonctionne telle quelle, notamment si elle repose sur la présence de [nginxproxy/nginx-proxy](https://github.com/nginx-proxy/nginx-proxy).

C'est en effet la technologie utilisée par Protocode pour gérer le routing vers les conteneurs. Une instance Nginx va capturer toutes les requêtes entrantes sur le port 443, et les rediriger sur le port 80 d'un conteneur s'il est associé à l'url de la requête.

Si un conteneur est censé être exposé (accessible sur une url), vous devrez lui ajouter les variables d'environnement suivantes:

| Variable | Description |
| --------- | ------------- |
| VIRTUAL_HOST | Cette variable définit une url à associer au conteneur. Vous pouvez y mettre n'importe quelle valeur, **elle sera remplacée lors de la construction de l'environnement** en fonction de l'url du serveur qui l'hébergera. Cette variable **permet juste par sa simple présence d'identifier un conteneur à exposer**. |
| VIRTUAL_PORT | Cette variable définit le port écouté par le conteneur. Ainsi les requêtes ne seront plus redirigées vers le port 80, mais vers le port ainsi défini. Si votre conteneur écoute le port 80, il est donc inutile d'ajouter cette variable. |

Voici un exemple d'utilisation de ces variables pour une application Node 16 écoutant le port 8080:

[prism classes="language-yaml line-numbers" highlight="6,7"]
version: "3"
services:
    app:
        image: "node:16"
        environment:
            VIRTUAL_HOST: "app-my-repository.${ENVIRONMENT_URL}"
            VIRTUAL_PORT: 8080
[/prism]

---

## Déterminisme du nom des conteneurs:

Tous les conteneurs sans noms prédéfinis (n'ayant pas de clé `_container_name_` dans le fichier docker-compose) seront renommés de cette manière: 

`SLUG_DU_REPOSITORY`_`NOM_DU_SERVICE`

!!! **Le slug du repository est la dernière partie de son url sans extension**, où tous les caractères spéciaux (., _, :, ...) sont remplacés par "-".
!!! Par exemple, pour le repository _githost.ext/me/my-repository.git_, le slug est donc _**my-repository**_.

!!! **Le nom du service est quant à lui la clé déclarée dans services au sein du fichier docker-compose**. Dans l'exemple précédent, c'est _app_.

Pour l'exemple précédent, le nom du conteneur serait donc **"my-repository_app"**, et il serait donc possible depuis un environnement d'exécuter une commande telle que:
[prism classes="language-bash command-line" cl-prompt="$"]
docker exec my-repository_app bash -c "npm install"
[/prism]

Bien que, vu que nous utilisons docker-compose, si nous trouvons à la racine du repository il serait aussi possible de faire ceci:
[prism classes="language-bash command-line" cl-prompt="$"]
docker-compose exec app bash -c "npm install"
[/prism]

---

## Déterminisme des urls des conteneurs:

Les urls des conteneurs sont déterminées par Protocode de la manière suivante:

`NOM_DU_SERVICE`-`SLUG_DU_REPOSITORY`.`URL_DE_L_ENVIRONNEMENT`

Les deux premières particules sont facilement déductibles.

La dernière, l'url de l'environnement est en revanche non prédictible. Elle dépendra d'un certain nombre de facteurs, notamment le namespace de l'environnement (e1, e2, ...), et l'url du serveur (xxxxxx.protocode.cloud). Néanmoins, la valeur de celle-ci sera accessible au sein de tout environnement démarré dans une variable `ENVIRONMENT_URL`.
Dans le cas où certains de vos services ont besoin de connaitre l'url d'un conteneur (un front javascript communiquant avec une api par exemple), il est donc tout à fait possible d'assigner des variables qui utilisent `ENVIRONMENT_URL`:

[prism classes="language-yaml line-numbers" highlight="9,15"]
# Dans le fichier docker-compose d'un repository githost.ext/me/my-repository.git
version: "3"

services:
    api:
        image: "php:8.0"
        environment:
            VIRTUAL_HOST: "will-be-replaced-at-runtime.local" # L'api doit être exposée
            FRONT_URL: "front-my-repository.${ENVIRONMENT_URL}" # Elle a besoin de l'url du front

    front:
        image: "node:16"
        environment:
            VIRTUAL_HOST: "will-be-replaced-at-runtime.local" # Le front doit être exposé
            API_URL: "api-my-repository.${ENVIRONMENT_URL}" # Il a besoin de l'url de l'api
[/prism]


---

## Les variables d'environnement

Il y en a une qui est injectée automatiquement par Protocode, c'est celle vue précédemment: `ENVIRONMENT_URL`. Elle est accessible depuis l'environnement, et il est possible d'y faire référence dans les fichiers docker-compose, comme vu dans l'exemple précédent.

Mais il est aussi possible d'ajouter des variables d'environnement au sein de la configuration de chaque source. Ces variables ne seront pas injectées dans l'environnement, car elles pourraient potentiellement entrer en conflit entre deux sources. Cependant, elles seront injectées dans toutes les commandes docker-compose. C'est notamment utile lorsque plusieurs services utilisent les mêmes variables.

Prenons l'exemple d'un conteneur faisant tourner PhpMyAdmin, et un conteneur faisant tourner une base MySql 8:
Les variables suivantes suivantes sont ajoutées:

| Nom | Valeur |
| ----- | ------- |
| DB_NAME | my_project_db |
| DB_USER | user |
| DB_PASSWORD | password |
| DB_PORT | 3306 |

Et elles seront automatiquement injectées lors de l'utilisation du fichier de configuration par docker-compose:

[prism classes="language-yaml line-numbers"]
version: "3"
services:
    database:
        image: "mysql:8.0"
        command: --default-authentication-plugin=mysql_native_password
        environment:
            - "MYSQL_DATABASE=${DB_NAME}"
            - "MYSQL_USER=${DB_USER}"
            - "MYSQL_PASSWORD=${DB_PASSWORD}"
            - "MYSQL_ROOT_PASSWORD=${DB_PASSWORD}"
            - "MYSQL_PORT=${DB_PORT}"
    phpmyadmin:
        image: phpmyadmin/phpmyadmin
        environment:
            - VIRTUAL_HOST=phpmyadmin.local
            - "MYSQL_USERNAME=${DB_USER}"
            - "MYSQL_PASSWORD=${DB_PASSWORD}"
            - "PMA_USERNAME=${DB_USER}"
            - "PMA_PASSWORD=${DB_PASSWORD}"
            - "PMA_HOSTS=database"
        links:
            - database
[/prism]
