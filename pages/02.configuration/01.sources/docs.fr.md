---
title: 'Ajout des repositories'
taxonomy:
    category:
        - docs
---

Dans Protocode, nous distinguons les projets et les repositories, bien qu'il arrive souvent ces notions se rejoignent. Le projet est une notion globale, qui englobe une ou plusieurs sources, mais aussi les tâches à faire et les membres de l'équipe.

Lorsqu'un projet est créé, la deuxième étape est d'ajouter les repositories. Pour les projets monobloc, il n'y en aura qu'un. Mais dans des projets complexes, il pourra y en avoir plusieurs. Par exemple, un repository avec une api, et un autre avec un site en javascript qui consomme l'api.

L'ajout d'un repository se fera en 3 étapes: 

* **L'identification**: le remplissage d'un nom et de l'url de celui-ci. L'url peut être celle d'un repository hébergé sur GitHub, mais aussi GitLab (public ou privé), ou encore BitBucket. Si cette url est en https, il faudra fournir dans l'url l'utilisateur et le mot de passe. _Par exemple https://user:password@githost.ext/me/my-repository.git_. C'est cependant fortement déconseillé, car ces identifiants de connexion seront visibles par toutes les personnes accédant aux environnements ouverts. Nous vous invitons à fournir à la place celle utilisée en ssh, _par exemple git@githost.ext:me/my-repository.git_. 

* **L'attribution des droits en lecture et écriture à Protocode**. Cette étape sera requise si l'url fournie est celle utilisée en ssh. Protocode va générer une clé ssh qu'il faudra reporter en tant que clé de déploiement ayant les droits en écriture. 

* **Le choix de la branche par défaut**: celle sur laquelle sera positionné l'utilisateur à l'ouverture d'un environnement.

Une fois ces étapes remplies, Protocode est capable de récupérer et pousser des modifications vers le code source, mais il n'est pas encore en mesure de l'exécuter. Cette partie relève de l'étape suivante: la virtualisation.

